/// <reference types = "Cypress"/>

describe('get api user test',()=>{
    it('get statusCode and log',()=>{
        cy.request({
            method: 'GET',
            url : 'http://localhost:3000/users',
            
        }).then((res)=>{
            expect(res.status).to.eq(200)
            cy.log(JSON.stringify(res))
        })
    })
    it('get user by id test',()=>{
        cy.request({
            method: 'GET',
            url : 'http://localhost:3000/users/1',
            
        }).then((res)=>{
            expect(res.status).to.eq(200)
            expect(res.body.firstName).to.eq('Arjun')
        })
    })
})