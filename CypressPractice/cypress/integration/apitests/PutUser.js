/// <reference types = "Cypress"/>

describe('Post user request', () => {

    it('Create User', () => {
//Creating a new User.
        cy.request({
            method: 'POST',
            url: 'http://localhost:3000/users',
            headers: {
                'content-type': 'application/json'
            },
            body: {
                "firstName": "Micheal",
                "lastName": "Jones",
                "subjectId": 1,
            }
        }).then((res) => {
            cy.log(JSON.stringify(res))
            expect(res.status).to.eq(201)
            expect(res.body).has.property('firstName', 'Micheal')
            expect(res.body).has.property('lastName', 'Jones')
            expect(res.body).has.property('subjectId', 1)
        }).then((res) => {
            const userId = res.body.id
            cy.log("user id is" + userId)
            //Updating the created user.
            cy.request({
                method: 'PUT',
                url: 'http://localhost:3000/users/' + userId,
                headers: {
                    'content-type': 'application/json'
                },
                body: {
                    "firstName": "Mary",
                    "lastName": "Jane",
                    "subjectId": 2
                }
            }).then((res) => {
                expect(res.status).to.eq(200)
                expect(res.body).has.property('firstName', 'Mary')
                expect(res.body).has.property('lastName', 'Jane')
                expect(res.body).has.property('subjectId', 2)
            })
        })
    });

})