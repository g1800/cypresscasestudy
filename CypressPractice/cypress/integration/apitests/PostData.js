/// <reference types = "Cypress"/>

const dataJSON= require('../../fixtures/createUser.json')
describe('Post user request', () => {

    let randomText = ""
    let testfirstName = ""
    it('Create User', () => {
        var pattern = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        for (var i = 0; i < 6; i++)
            randomText += pattern.charAt(Math.floor(Math.random() * pattern.length));
        testfirstName = randomText

        //Create A New User.
        cy.request({
            method: 'POST',
            url: 'http://localhost:3000/users',
            headers: {
                'content-type': 'application/json'
            },
            body: {
                "firstName": testfirstName,
                "lastName": dataJSON.lastName,
                "subjectId": dataJSON.subjectId,
            }
        }).then((res) => {
            cy.log(JSON.stringify(res))
            expect(res.status).to.eq(201)
            expect(res.body).has.property('firstName', testfirstName)
            expect(res.body).has.property('lastName', dataJSON.lastName)
            expect(res.body).has.property('subjectId', dataJSON.subjectId)
        })
    });

})