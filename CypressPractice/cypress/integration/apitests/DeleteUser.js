/// <reference types = "Cypress"/>

describe('Post user request', () => {

    it('Create User', () => {
        //Create a New User
        cy.request({
            method: 'POST',
            url: 'http://localhost:3000/users',
            headers: {
                'content-type': 'application/json'
            },
            body: {
                "firstName": "New",
                "lastName": "User",
                "subjectId": 1
            }
        }).then((res) => {
            cy.log(JSON.stringify(res))
            expect(res.status).to.eq(201)
            expect(res.body).has.property('firstName', 'New')
            expect(res.body).has.property('lastName', 'User')
            expect(res.body).has.property('subjectId', 1)
        }).then((res) => {
            const userId = res.body.id
            cy.log("user id is" + userId)
            //Delete the created User
            cy.request({
                method: 'DELETE',
                url: 'http://localhost:3000/users/' + userId,
                headers: {
                    'content-type': 'application/json'
                }
            }).then((res) => {
                expect(res.status).to.eq(200)
            })
        })
    });

})